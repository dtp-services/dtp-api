const $validtor = require('jsonschema').validate;
const $Posts = require('../modules/posts');
const $log = require('../libs/log');
const _ = require('lodash');

const ERRORS = require('../references/errors');
const SCHEMA = require('../schemes/get-trending');
const moduleName = 'handlers.get-trending';
const authHandler = require('../middlewares/auth');

module.exports = function (app) {
  app.post('/getTrending', (req, res, next) => {
    req.allowEmptyToken = true;
    next();
  }, authHandler, (req, res) => {
    const valid = $validtor(req.body, SCHEMA);

    if (valid.errors.length > 0) {
      return res.json({
        ok: false,
        error: {
          msg: 'invalid params',
          data: valid.errors
        }
      });
    }
    
    $Posts.getTrending(_.get(req, 'session.user.id', false), req.body)
      .then((result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s] error from create post', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};