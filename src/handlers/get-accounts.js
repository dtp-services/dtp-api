const $Users = require('../modules/users');
const $log = require('../libs/log');

const moduleName = 'handlers.get-accounts';

const ERRORS = require('../references/errors');
const authCheck = require('../middlewares/auth');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/getAccounts', authCheck, (req, res) => {

    $Users.getAccounts(req.session.user.id)
      .then(async (result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s] error from get accounts', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};