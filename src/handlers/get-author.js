const $Users = require('../modules/users');
const $log = require('../libs/log');

const moduleName = 'handlers.get-author';

const ERRORS = require('../references/errors');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/getAuthor/:username', (req, res) => {
    const username = req.params.username;

    $Users.getAuthor(username)
      .then(async (result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s]', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};