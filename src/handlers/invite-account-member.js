const $Users = require('../modules/users');
const $log = require('../libs/log');

const moduleName = 'handlers.invite-account-member';

const ERRORS = require('../references/errors');
const authCheck = require('../middlewares/auth');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/inviteAccountMember/*', authCheck, (req, res) => {
    const inviteId = req.params[0];

    $Users.inviteAccountMember(req.session.user.id, inviteId)
      .then(async (result) => {
        res.json({
          ok: true
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s]', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};