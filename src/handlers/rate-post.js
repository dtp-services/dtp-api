const $validtor = require('jsonschema').validate;
const $Posts = require('../modules/posts');
const $log = require('../libs/log');
const moduleName = 'handlers.rate-post';

const _ = require('lodash');

const SCHEMA = require('../schemes/rate-post');
const ERRORS = require('../references/errors');
const authHandler = require('../middlewares/auth');

module.exports = function (app) {
  app.post('/ratePost', authHandler, (req, res) => {
    const valid = $validtor(req.body, SCHEMA);

    if (valid.errors.length > 0) {
      return res.json({
        ok: false,
        error: {
          msg: 'invalid params',
          data: valid.errors
        }
      });
    }

    $Posts.rate(Object.assign({
      user_id: req.session.user.id
    }, req.body))
      .then((result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s] error from rate post', moduleName, err);
        }

        if (_.get(err, 'error') === 'PAGE_NOT_FOUND') {
          return res.json({
            ok: false,
            error: ERRORS['post-source-invalid']
          });
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};