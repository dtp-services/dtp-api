const $validtor = require('jsonschema').validate;
const $Posts = require('../modules/posts');
const $log = require('../libs/log');
const moduleName = 'handlers.drop-post';

const _ = require('lodash');

const SCHEMA = require('../schemes/drop-post');
const ERRORS = require('../references/errors');
const authHandler = require('../middlewares/auth');
const accountAccessCheck = require('../middlewares/account-access');

module.exports = function (app) {
  app.post('/dropPost', authHandler, (req, res, next) => {
    req.skipEmptyAccountId = true;
    next();
  }, accountAccessCheck, (req, res) => {
    if (req.session.user.ban) {
      return res.json({
        ok: false,
        error: ERRORS['user-ban']
      });
    }

    const valid = $validtor(req.body, SCHEMA);

    if (valid.errors.length > 0) {
      return res.json({
        ok: false,
        error: {
          msg: 'invalid params',
          data: valid.errors
        }
      });
    }

    $Posts.create(Object.assign({
      user_id: req.session.user.id
    }, req.body))
      .then((result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s] error from create post', moduleName, err);
        }

        if (_.get(err, 'error') === 'PAGE_NOT_FOUND') {
          return res.json({
            ok: false,
            error: ERRORS['post-source-invalid']
          });
        } else if (err === 'SMALL_CONTENT') {
          return res.json({
            ok: false,
            error: ERRORS['small-content']
          });
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};