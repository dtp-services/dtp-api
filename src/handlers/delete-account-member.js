const $Users = require('../modules/users');
const $log = require('../libs/log');
const $validtor = require('jsonschema').validate;


const moduleName = 'handlers.delete-account-member';

const ERRORS = require('../references/errors');
const authCheck = require('../middlewares/auth');
const accountAccessCheck = require('../middlewares/account-access');
const SCHEMA = require('../schemes/delete-account-member');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/deleteAccountMember/:account_id/', authCheck, accountAccessCheck, (req, res) => {
    const accountId = req.params.account_id;
    const valid = $validtor(req.body, SCHEMA);

    if (valid.errors.length > 0) {
      return res.json({
        ok: false,
        error: {
          msg: 'invalid params',
          data: valid.errors
        }
      });
    }

    $Users.deleteAccount(req.body.user_id, accountId)
      .then(async (result) => {
        res.json({
          ok: true
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s]', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};