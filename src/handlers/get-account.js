const $Users = require('../modules/users');
const $log = require('../libs/log');

const moduleName = 'handlers.get-account';

const ERRORS = require('../references/errors');
const authCheck = require('../middlewares/auth');
const accountAccessCheck = require('../middlewares/account-access');

const _ = require('lodash');

module.exports = function (app) {
  app.post('/getAccount/:account_id', authCheck, accountAccessCheck, (req, res) => {
    const accountId = req.params.account_id;

    $Users.getAccount(accountId)
      .then(async (result) => {
        res.json({
          ok: true,
          result
        });
      })
      .catch((err) => {
        if (!_.get(err, 'Error')) {
          $log.error('[%s]', moduleName, err);
        }

        res.json({
          ok: false,
          error: !_.get(err, 'Error') ? ERRORS['unknown'] : err.Error
        });
      });
  });
};