const $cors = require('cors');

const corsOptions = {
  origin: function (origin, cb) {
    cb(null, true);
  }
};

module.exports = function (app) {
  app.use($cors(corsOptions))
};