const bodyParser = require('body-parser');

module.exports = function (app) {
  app.use(bodyParser.json({
    limit: '3mb'
  }));

  app.use((err, req, res, next) => {
    if (err) {
      console.log(err);

      return res.end('Server Error');
    }

    next();
  });
};