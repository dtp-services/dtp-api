const $Users = require('../modules/users');
const $log = require('../libs/log');

const _ = require('lodash');
const ERRORS = require('../references/errors');

module.exports = function (req, res, next) {
  const token = _.get(req, 'body.token');

  if (!token && !req.allowEmptyToken) {
    return res.json({
      ok: false,
      error: ERRORS['auth-failed']
    });
  } else if (!token && req.allowEmptyToken) {
    return next();
  }

  $Users.getBySecretToken(token)
    .then((user) => {
      if (!user) {
        throw 'auth-failed';
      }

      _.set(req, 'session.user', {
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
        ban: user.ban
      });

      return $Users.setSession(user.id);
    })
    .then(() => {
      next();
    })
    .catch((err) => {
      if (err === 'auth-failed') {
        return res.json({
          ok: false,
          error: ERRORS['auth-failed']
        });
      }

      $log.error(err);
      res.json({
        ok: false,
        error: ERRORS['unknown']
      });
    });
};