const $AF = require('../libs/action-flow');
const $mongo = require('../libs/mongo');
const $log = require('../libs/log');
const $uuid = require('uuid/v4');

class ShortID {
  static get postsDB () {
    return $mongo.collection('posts');
  }

  static get groupAccountsDB () {
    return $mongo.collection('groupAccounts');
  }

  static async gen () {
    const uuid = $uuid().split('-');
    const vars = [uuid[1], uuid[2], uuid[3]];
    
    vars.push(...vars.filter((item) => {
      if (item.match(/^([0-9]+)$/)) {
        return false;
      } else {
        return true;
      }
    }).map((item) => item.toUpperCase()));

    const actionFlow = $AF.multi(vars.map((item) => {
     return {
      desc: 'check short',
      v: item
     };
    }));

    async function cancel (err) {
      await actionFlow.end();

      if (err) {
        throw err;
      }
    }

    await actionFlow.await();

    const existsIds = [];
    const checkList = await ShortID.postsDB.find({
      shortId: {$in: vars}
    }, {
      project: {
        shortId: 1,
        _id: 0
      }
    }).catch(cancel);

    const accountsByUsername = await ShortID.groupAccountsDB.find({
      username: {$in: vars}
    }).catch(cancel);

    existsIds.push(...accountsByUsername.map((item) => item.username));
    existsIds.push(...checkList.map((item) => item.shortId));

    const id = vars.find((item) => !existsIds.includes(item));

    await cancel();

    if (!id) {
      throw 'ERROR_GEN_SHORT_ID';
    }

    return id;
  }
}

module.exports = ShortID;

if (process.env.TEST) {
  ShortID.gen()
    .then((sh) => {
      console.log(sh);
      process.exit();
    })
    .catch((err) => {
      console.log(err);
      process.exit();
    })
}