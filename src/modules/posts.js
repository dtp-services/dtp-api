const $mongo = require('../libs/mongo');
const $AF = require('../libs/action-flow');
const $uuid = require('uuid/v4');
const $TelegraphAPI = require('../modules/telegraph-api');
const $TelegraphAggregator = require('../modules/telegraph-aggregator');
const $crc32 = require('crc-32');
const $log = require('../libs/log');
const $Promise = require('bluebird');
const $escapeRegExp = require('escape-string-regexp');

const _ = require('lodash');

const ERRORS = require('../references/errors');

function postsMap (post) {
  const postVotes = post.votes || {
    up: 0,
    down: 0,
    total: 0
  };

  postVotes.down = Math.abs(postVotes.down);

  return {
    short_id: post.shortId,
    date: post.createdDate,
    preview: post.preview,
    votes: postVotes
  }
}

function postsMapStudio (post) {
  const postVotes = post.votes || {
    up: 0,
    down: 0,
    total: 0
  };

  postVotes.down = Math.abs(postVotes.down);

  return {
    short_id: post.shortId,
    date: post.createdDate,
    preview: post.preview,
    votes: postVotes,
    controlKey: post.controlKey
  }
}

class Posts {
  static get db () {
    return $mongo.collection('posts');
  }

  static get dbContent () {
    return $mongo.collection('contentIndex');
  }

  static get dbRates () {
    return $mongo.collection('rates');
  }

  static get groupAccountsDB () {
    return $mongo.collection('groupAccounts');
  }

  static get contentCacheDB () {
    return $mongo.collection('contentCache');
  }

  static async check (path, session = {}) {
    const exists = await Posts.db.findOne({ path });

    if (exists) {
      return {
        exists: true,
        short_id: exists.shortId
      };
    }

    const content = await Posts.checkPostContent(path)
      .catch((err) => {
        if (err === 'SMALL_CONTENT') {
          throw {Error: ERRORS['small-content']};
        }
      });

    if (!content) {
      throw {Error: ERRORS['post-source-invalid']};
    }

    const Language = require('./language');
    const language = await Language.getCode(content);

    const result = {
      exists: false,
      language
    };

    const userId = _.get(session, 'user.id');

    if (!userId) {
      return result;
    }

    result.account = await Posts.getPostOwnerAccount(userId, path);

    return result;
  }

  static async getPostOwnerAccount (userId, path) {
    const Users = require('./users');
    const accounts = await Users.getAccounts(userId);

    if (accounts.length === 0) {
      return false;
    }

    const accountInfo = await Posts.groupAccountsDB.find({
      id: {$in: accounts.map((item) => item.id)}
    });

    const tokens = accountInfo.map((account) => account.token);
    const accountOwner = await Posts.checkPermissionsToPost(path, tokens)
      .then((result) => {
        const data = Object.keys(result).reduce((account, token) => {
          if (result[token]) {
            account = accountInfo.find((item) => item.token === token);
          }

          return account;
        }, false);

        return data;
      });

    return accountOwner && _.pick(accountOwner, ['id', 'name']) || false;
  }

  static async checkPermissionsToPost (path, token) {
    const tokenList = [];

    if (!(token instanceof Array)) {
      tokenList.push(token);
    } else {
      tokenList.push(...token);
    }

    const post = await $TelegraphAPI.query('getPage', {
      path,
      return_content: true
    })
      .then((data) => {
        if (!data.ok) {
          throw data;
        }

        return data.result;
      });

    return $Promise.props(tokenList.reduce((r, token) => {
      r[token] = $TelegraphAPI.query('editPage', {
        access_token: token,
        path: post.path,
        title: post.title,
        content: post.content,
        author_name: post.author_name,
        author_url: post.author_url,
        return_content: false
      })
        .then((data) => {
          return Number(data.ok);
        });

      return r;
    }, {}));
  }

  static async create (data) {
    const path = data.path.split('.ph/').pop();
    const actionFlow = $AF.create({
      description: 'drop post',
      path
    });

    async function cancel (err) {
      await actionFlow.end();

      if (err) {
        throw err;
      }
    }

    await actionFlow.await();
    const exists = await Posts.db.count({path}).catch(cancel);

    if (exists) {
      return cancel({
        Error: ERRORS['post-exists']
      });
    }

    const shortId = await Posts.genShortLink().catch(cancel);
    await Posts.createContentIndex(path, shortId).catch(cancel);

    const controlKey = $uuid();
    const createdDate = Math.round((new Date()).getTime() / 1000);
    const preview = await Posts.getPreview(path).catch(cancel);
    const tags = data.tags.map((t) => {
      return t.replace(/^#/g, '');
    }).reduce((r, w) => {
      if (!r.find((item) => item.toLowerCase() === w.toLowerCase())) {
        r.push(w);
      }

      return r;
    }, []);
    const tags_index = tags.join(' ').toLowerCase();

    const insertData = {
      path,
      shortId,
      preview,
      tags,
      tags_index,
      language: data.language,
      controlKey,
      createdDate,
      votes: {
        up: 0,
        down: 0,
        total: 0
      }
    };

    if (data.account_id) {
      insertData.account_id = data.account_id;
    } else {
      insertData.user_id = data.user_id;
    }

    const insert = await Posts.db.insert(insertData).catch(cancel);

    cancel();

    return {
      controlKey,
      shortId,
      preview
    };
  }

  static async checkPostContent (path) {
    const loadContent = await $TelegraphAPI.query('getPage', {
      path,
      return_content: true
    });

    // for err SMALL_CONTENT
    const minLength = 900; // symbols

    if (!loadContent.ok) {
      throw loadContent;
    }

    const rawContent = loadContent.result.content;

    function jsonContentToText (arr) {
      return arr.reduce((r, item) => {
        if (typeof item === 'string') {
          r += item;
        } else if (item.children) {
          r += jsonContentToText(item.children);
        }

        return r;
      }, '');
    }

    const content = loadContent.result.title + ' ' + jsonContentToText(rawContent);

    if (content.length < minLength) {
      throw 'SMALL_CONTENT';
    }

    return content;
  }

  static async createContentIndex (path, shortId) {
    const createdDate = Math.round((new Date()).getTime() / 1000);
    const content = await Posts.checkPostContent(path);

    await Posts.dbContent.insert({
      createdDate,
      shortId,
      content
    });
  }

  static async getPreview (path) {
    const data = await $TelegraphAPI.query('getPage', {
      path, return_content: false
    });

    if (data.ok) {
      return data.result;
    } else {
      throw data;
    }
  }

  static async getLatest (userId, params) {
    let Users;

    if (userId) {
      Users = require('./users');
    }

    let filter = {};
    const offset = _.get(params, 'offset', 0);

    if (userId) {
      const trendFilter = await Users.getTrendingFilter(userId);

      if (trendFilter.tags.length) {
        filter.$text = {
          $search: trendFilter.tags.reduce((r, tag) => {
            tag = tag.replace(/^#/, '').toLowerCase();

            r += tag + ' ';

            return r;
          }, '')
        };
      }

      if (trendFilter.language.length !== 0) {
        filter.language = {$in: trendFilter.language}; 
      }
    }

    let list = await Posts.db.find(filter, {
      sort: {
        createdDate: -1,
      },
      skip: offset,
      limit: 10
    });

    return list.map(postsMap);
  
  }

  static async getTrending (userId, params) {
    let Users;

    if (userId) {
      Users = require('./users');
    }

    let filter = {};
    const offset = _.get(params, 'offset', 0);

    if (userId) {
      const trendFilter = await Users.getTrendingFilter(userId);

      if (trendFilter.tags.length) {
        filter.$text = {
          $search: trendFilter.tags.reduce((r, tag) => {
            tag = tag.replace(/^#/, '').toLowerCase();

            r += tag + ' ';

            return r;
          }, '')
        };
      }

      if (trendFilter.language.length !== 0) {
        filter.language = {$in: trendFilter.language};
      }
    }

    let list = await Posts.db.find(filter, {
      sort: {
        'votes.total': -1,
        'preview.views': -1
      },
      skip: offset,
      limit: 10
    });

    const tags = list.reduce((r, post) => {
      post.tags.forEach((tag) => {
        if (!r.find((item) => item.toLowerCase() === tag.toLowerCase())) {
          r.push(tag);
        }
      });

      return r;
    }, []);

    return {
      posts: list.map(postsMap),
      tags: tags.map((tag) => {
        if (!tag.match(/^#/)) {
          return {tag: '#' + tag};
        } else {
          return {tag};
        }
      })
    };
  }

  static async getPostContent (path) {
    const expired = 900;
    const now = Math.round( (new Date()).getTime() / 1000 );
    const timeExpired = now - expired;
    const cache = await Posts.contentCacheDB.findOne({
      path,
      time: {$gt: timeExpired}
    });

    if (cache) {
      return cache.content;
    }

    const content = await $TelegraphAPI.query('getPage', {
      path,
      return_content: true
    })
      .then((data) => {
        if (!data.ok) {
          throw data;
        }

        return data.result.content;
      });

    await Posts.contentCacheDB.insert({
      path,
      content,
      time: now,
      date: new Date()
    });

    return content;
  }

  static async getByShortId (shortId) {
    const post = await Posts.db.findOne({ shortId });

    if (!post) {
      return;
    }

    const currTime = Math.round( (new Date()).getTime() / 1000 );
    const previewTimeout = 300; // sec // 5 min

    const offset = currTime - _.get(post, 'preview.updatedDate', 0);

    if (offset > previewTimeout) {
      post.preview = await Posts.updatePreview(post.path);
    }

    return post;
  }

  static async updatePreview (path) {
    const newPreview = await Posts.getPreview(path);
    const updatedDate = Math.round( (new Date()).getTime() / 1000 );

    newPreview.updatedDate = updatedDate;

    await Posts.db.editOne({ path }, {
      preview: newPreview
    });

    return newPreview;
  }

  static async deleteByControlKey (accountId, controlKey) {
    const deleteFilter = {
      controlKey
    };

    if (typeof accountId === 'number') {
      deleteFilter.user_id = accountId;
    } else {
      deleteFilter.account_id = accountId;
    }

    const post = await Posts.db.findOne(deleteFilter, {
      fields: {
        short_id: 1
      }
    });

    if (!post) {
      throw {Error: ERRORS['post-source-invalid']};
    }

    await Posts.dbContent.deleteOne({ short_id: post.short_id });
    return await Posts.db.deleteOne(deleteFilter);
  }

  static async genShortLink () {
    const $ShortID = require('./short-id');

    return await $ShortID.gen();
  }

  static async rate (data) {
    const AF = $AF.create({
      description: 'rate post',
      shortId: data.shortId
    });

    async function cancel (err) {
      await AF.end();

      if (err) {
        $log.error(err);
        throw err;
      }
    }

    await AF.await();

    const userId = data.user_id;
    const post = await Posts.db.findOne({ shortId: data.shortId }, {
      fields: {
        _id: 0,
        votes: 1
      }
    }).catch(cancel);
    const lastRate = await Posts.dbRates.findOne({
      user_id: userId,
      shortId: data.shortId
    }).catch(cancel);

    if (lastRate) {
      if (lastRate.delta === data.delta) {
        return cancel();
      } else {
        // cancel last rate
        post.votes.total -= lastRate.delta;
        post.votes[lastRate.delta < 0 ? 'down' : 'up'] -= lastRate.delta;
      }
    }

    // add new rate
    post.votes.total += data.delta;
    post.votes[data.delta < 0 ? 'down' : 'up'] += data.delta;

    const date = Math.round( (new Date()).getTime() / 1000 );
    const promises = [];

    if (lastRate) {
      promises.push( Posts.dbRates.editOne({ _id: lastRate._id }, {
        delta: data.delta
      }).catch(cancel) );
    } else {
      promises.push( Posts.dbRates.insert({
        user_id: userId,
        shortId: data.shortId,
        delta: data.delta,
        date
      }).catch(cancel) );
    }

    promises.push( Posts.db.editOne({ shortId: data.shortId }, {
      votes: post.votes
    }).catch(cancel) );

    await $Promise.all(promises).catch(cancel);
    return cancel();
  }

  static async searchPost (params) {
    const query = params.query;
    const tags = params.tags;
    const offset = params.offset || 0;

    let list, posts, filter;

    if (query) {
      list = await Posts.dbContent.find({
        $text: {
          $search: `"${query.replace('"', '\\"')}"`
        }
      }, {
        limit: 15,
        sort: {
          createdDate: -1
        },
        skip: offset
      });

      filter = { shortId: { $in: list.map((item) => item.shortId ) } };
    } else if (tags) {
      filter = {
        $text: {
          $search: tags
        }
      };
    }

    if (query) {
      posts = await Posts.db.find(filter);
    } else if (tags) {
      posts = await Posts.db.find(filter, {
        limit: 15,
        skip: offset
      });
    }

    return posts.map(postsMap);
  }

  static async getListByAuthor (username, options = {}) {
    username = username.toLowerCase();
    
    const account = await Posts.groupAccountsDB.findOne({
      username
    });

    if (!account) {
      throw {Error: ERRORS['invalid-account-id']};
    }

    const account_id = account.id;
    const filter = {
      account_id
    };

    if (options.query) {
      filter['preview.title'] = new RegExp($escapeRegExp(String(options.query), 'i'));
    }

    const list = await Posts.db.find(filter, {
      limit: 15,
      skip: Number(options.offset || 0),
      sort: {
        createdDate: -1
      }
    });

    return list.map(postsMap);
  }

  static async getListByAccount (accountId, options = {}) {
    const accountIsUser = !!Number(accountId);

    if (!accountIsUser && options.telegraph) {
      const account = await Posts.groupAccountsDB.findOne({
        id: accountId
      });

      if (!account) {
        throw {Error: ERRORS['invalid-account-id']};
      }

      return await $TelegraphAggregator.query('list/' + account.token, options)
        .then((data) => {
          if (!data.ok) {
            if (_.get(data, 'error.code') === 7) {
              throw {Error: ERRORS['denied-account']};
            }

            throw data;
          }

          return data.result;
        });
    }

    const filter = {};

    if (accountIsUser) {
      filter.user_id = Number(accountId);
    } else {
      filter.account_id = String(accountId);
    }

    if (options.query) {
      filter['preview.title'] = new RegExp($escapeRegExp(String(options.query)), 'i');
    }

    const list = await Posts.db.find(filter, {
      limit: 15,
      skip: Number(options.offset || 0),
      sort: {
        createdDate: -1
      }
    });

    return list.map(postsMapStudio);
  }
}

module.exports = Posts;
