const Schema = require('../../config/system/schema');

module.exports = {
  "type": "object",
  "properties": {
    "short_name": {
      "type": "string",
      "maxLength": 32
    },
    "author_name": {
      "type": "string",
      "maxLength": 128
    },
    "author_url": {
      "type": "string",
      "maxLength": 512
    },
    "transfer": {
      "type": "string",
      "minLength": 20
    }
  },
  "oneOf": [
    {"required": ["short_name"]},
    {"required": ["transfer"]},
  ]
};