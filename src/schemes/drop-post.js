const Schema = require('../../config/system/schema');

module.exports = {
  "type": "object",
  "properties": {
    "path": Schema.path,
    "language": Schema.language,
    "tags": Schema.tags,
    "account_id": Schema.uuid
  },
  "required": ["path", "language", "tags"]
};