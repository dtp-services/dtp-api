const Schema = require('../../config/system/schema');

module.exports = {
  "type": "object",
  "properties": {
    "username": Schema.authorUsername
  },
  "required": ["username"]
};