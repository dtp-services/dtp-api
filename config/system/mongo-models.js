const models = {};

const Schema = require('./schema');

models['__af__queue'] = {};

models['posts'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      account_id: Schema.uuid,
      shortId: Schema.short_id,
      path: Schema.path,
      preview: Schema.preview,
      tags: Schema.tags,
      tags_index: Schema.anyString,
      language: Schema.language,
      controlKey: Schema.uuid,
      createdDate: Schema.anyNumber,
      votes: Schema.votes
    },
    required: ['path', 'tags', 'language', 'votes']
  }
};

models['users'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      id: Schema.user_id,
      first_name: Schema.anyString,
      last_name: Schema.anyString,
      photo: Schema.userPhoto,
      last_bot_session: Schema.anyNumber,
      last_api_session: Schema.anyNumber,
      secretToken: Schema.anyString,
      openToken: Schema.anyString
    },
    required: ['id', 'first_name', 'last_name']
  }
};

models['postDrafts'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      account_id: Schema.uuid,
      path: Schema.path,
      tags: Schema.tags,
      language: Schema.language,
      createdDate: Schema.anyNumber,
      controlKey: Schema.uuid,
      done: {
        type: 'boolean'
      }
    },
    required: ['user_id', 'path', 'done', 'createdDate']
  }
}

models['userScript'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      cmd: Schema.anyString,
      payload: Schema.anyObject
    },
    required: ['user_id', 'cmd']
  }
};

models['rates'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      shortId: Schema.short_id,
      delta: Schema.voteDelta,
      date: Schema.anyNumber
    },
    required: ['user_id', 'shortId', 'delta', 'date']
  }
};

models['sessions'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      sessionid: Schema.anyString,
      data: Schema.anyObject
    },
    required: ['sessionid', 'data']
  }
}

models['contentIndex'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      createdDate: Schema.anyNumber,
      shortId: Schema.short_id,
      content: Schema.anyString
    },
    required: ['createdDate', 'shortId', 'content']
  }
};

models['groupAccounts'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      id: Schema.uuid,
      username: Schema.authorUsername,
      name: {
        type: 'string',
        maxLength: 128
      },
      token: Schema.anyString,
      createdDate: Schema.anyNumber
    },
    required: ['id', 'name', 'createdDate', 'token']
  }
};

models['groupAccountsMembers'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      account_id: Schema.uuid,
      access: Schema.anyNumber
    },
    required: ['user_id', 'account_id', 'access']
  }
};

models['trendingFilters'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      language: Schema.languages,
      tags: {
        type: 'array',
        items: Schema.tag,
        minItems: 0,
        maxItems: 10
      }
    },
    required: ['user_id', 'language', 'tags']
  }
};

models['groupAccountsInvites'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      id: Schema.uuid,
      account_id: Schema.uuid
    },
    required: ['id', 'account_id']
  }
};

models['banList'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      path: Schema.anyString,
      userFrom: Schema.user_id,
      reason: Schema.anyString,
      createdDate: Schema.anyNumber,
      blockAuthor: {
        type: 'array',
        items: Schema.user_id
      }
    },
    required: ['path', 'userFrom', 'reason', 'createdDate']
  }
};

models['telegraphTokens'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      account_id: Schema.uuid,
      token: Schema.anyString,
      createdDate: Schema.anyNumber
    },
    required: ['user_id', 'account_id', 'token', 'createdDate']
  }
};

models['authorNamesSandbox'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      username: Schema.authorUsername,
      createdDate: Schema.anyNumber
    },
    required: ['user_id', 'username', 'createdDate']
  }
};

models['contentCache'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      path: Schema.path,
      content: {
        type: 'array'
      },
      time: Schema.anyNumber,
      date: {
        bsonType: 'date'
      }
    },
    required: ['path', 'content', 'time', 'date']
  }
};

models['contentBlacklist'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      type: Schema.anyString,
      value: Schema.anyString
    },
    required: ['type', 'value']
  }
};

models['postCovers'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      sourceFile: Schema.anyURL,
      coverFile: Schema.anyURL,
      contentHash: Schema.anyString,
      createdDate: Schema.anyNumber
    },
    required: ['sourceFile', 'coverFile', 'contentHash', 'createdDate']
  }
};
 
module.exports = models;
