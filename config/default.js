const PORT = require('../../dtp-ecosystem/reference/port');
const ENV = process.env.NODE_ENV;

module.exports = {
  server: {
    port: PORT['dtp-api'][ENV],
    middlewares: require('./system/middlewares'),
    handlers: require('./system/handlers')
  },
  mongo: {
    db: 'DTP-Database',
    models: require('./system/mongo-models'),
    log: process.env.LOG_LEVEL || 'info'
  },
  services: {
    publisherBot: {
      username: ''
    },
    telegraphAggregator: {
      url: 'http://127.0.0.1:' + PORT['telegraph-aggregator'][ENV]
    },
    yandex: {
      apiKey: ''
    },
    dtp: {
      url: ''
    },
    shortHost: {
      url: ''
    }
  }
};
